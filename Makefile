############### Makefile #######################################################

############### Source, object & binary files path #############################
srcdir := src
objdir := /tmp
bindir := bin

############### Names  #########################################################
program := test
modulename := msu-dpois0cufft3d
fortran_compiler := gfortran

############### Libraries ######################################################
# \note "-L/usr/local/cuda/lib64" is required for -lcudart -lcufft (only gfortran)
# \note "-lcudart" is required for cudaMalloc() 
lib := -L/usr/local/cuda/lib64 -lcudart -lcufft /usr/lib64/libstdc++.so.6

############### Object files names #############################################
objectname_cuda := $(modulename)-cuda
objectname_f90  := $(modulename)-f90
objects := $(objdir)/$(objectname_cuda).o $(objdir)/$(objectname_f90).o

############### CUDA code generation flags #####################################
GENCODE_SM10    := -gencode arch=compute_10,code=sm_10
GENCODE_SM20    := -gencode arch=compute_20,code=sm_20
GENCODE_SM30    := -gencode arch=compute_30,code=sm_30 
GENCODE_SM35    := -gencode arch=compute_35,code=sm_35
GENCODE_FLAGS   := $(GENCODE_SM20) $(GENCODE_SM30) $(GENCODE_SM35)

############### Flags ##########################################################
# Use -DNOOTPUT for no writing data to output file.
# Use -DCPUTIMER for CPU time measurement.
# Use -DGPUTIMER for GPU time measurement.
# Use -DTALKATIVE for additional output.
# Use -DGPUTIMER_DETAILED for detailed GPU time measurement 
#                                                    (use with -DGPUTIMER only!)
# Use -DTALKATIVE for additional output.

debugflags := -g -DTALKATIVE  
profflags  := -pg -DCPUTIMER -DGPUTIMER -DGPUTIMER_DETAILED 
optflags   := -O2

#extraflags := -DNOOUTPUT $(debugflags) $(profflags) $(optflags) # Full
#extraflags := -DNOOUTPUT $(optflags) # Fastest
extraflags := $(debugflags) $(profflags) # Debug

fortran_flags := -Wall -Werror -cpp $(extraflags)
nvcc_flags := $(GENCODE_FLAGS) $(extraflags)

############### Targets ########################################################
world: $(program)

$(program): $(srcdir)/$(program).f90 $(objectname_cuda) $(objectname_f90)
	$(fortran_compiler) $(fortran_flags) -o $(bindir)/$(program) $(srcdir)/$(program).f90 $(objects) $(lib)
	cp $(bindir)/$(program) $(program)

$(objectname_cuda): $(srcdir)/$(modulename).cu 
	nvcc  $(nvcc_flags) -c $(srcdir)/$(modulename).cu -o $(objdir)/$(objectname_cuda).o 

$(objectname_f90): $(srcdir)/$(modulename).f90 
	$(fortran_compiler) $(fortran_flags) -c $(srcdir)/$(modulename).f90 -o $(objdir)/$(objectname_f90).o

clean:
	rm -f *.o *.mod *.2 $(program) 

