!===============================================================================
!> @file test.f90
!! @brief Testing of Poisson equation cufft solver.
!! @author Nikolai Nikishin, <truecoldfault@gmail.com>
!! @copyright Department of the Automation for Scientific Research CMC MSU
!! @date 2013
!! @warning Compiler should support iso_c_binding module (Fortran 2003).
!! Gfortran 4.7.2 fits.
!! @todo Can we transfer arrays to C-function in more fancy way?
!! @todo 3dimensional output for XDRAW (in test.f90)
!! @todo Add another more complex function to tests.
!===============================================================================

!===============================================================================
!> Main testing program.
!! @brief Runs all the tests.
!===============================================================================
program test
  use iso_c_binding
  use msu_dpois0cufft3d_mod
  implicit none

  character (18), parameter :: STR_FAIL = "["//achar(27)// &
       &  "[91m FAIL!"//achar(27)//"[0m]"
  character (16), parameter :: STR_OK = "["//achar(27)// &
       & "[92m OK!"//achar(27)//"[0m]"
  double precision, parameter :: PI = 4.d0*datan(1.d0)
  double precision, parameter :: EPS = 0.06
  logical :: res = .FALSE.
  integer :: testnum

  !================================= Main cycle =================================
  do testnum = 1,1
#ifdef TALKATIVE
     print "(A,I2,A)","Starting test #",testnum,". Good luck!"
#endif
     select case(testnum)
     case (1)
        !============================== Test #01 ================================
        res = test01()
        !========================================================================
     end select

#ifdef TALKATIVE
     if (res .eqv. .TRUE.) then 
        print '(A,"Test #",I1," passed!")', STR_OK, testnum
     else
        print '(A,"Test #",I1," failed!")', STR_FAIL, testnum
     endif
#endif
  end do
  !========================== End of main cycle =================================

contains 
!================================================================================
!> Test #01.
!! @brief Solves poisson equation with f = sin(a1*pi*i/NX) * sin(a2*pi*j/NY) &
!!                                               & * sin(a3*pi*k/NZ)
!! and Dirichlet's boundary conditions in cubic area (LxLxL) on regular 
!! mesh (NxNxN). Solver executes several (T) times. Problem parameters (N, L, T) 
!! are passed as function arguments. Result is compared with analytical solution. 
!! @retval .TRUE. if test passed,
!! @retval .FALSE. otherwise.
!================================================================================
!================================================================================
!> Test #01.
!! @brief Solves poisson equation with f = sin(a1*pi*i/NX) * sin(a2*pi*j/NY) &
!!                                               & * sin(a3*pi*k/NZ)
!! and Dirichlet's boundary conditions in rectangular area (LXxLYxLZ) on grid 
!! (NXxNYxNZ), where grid dimensions are powers of two. 
!! Result is compared with analytical solution. 
!! @retval .TRUE. if test passed,
!! @retval .FALSE. otherwise.
!================================================================================

  function test01()
    implicit none
    
    logical :: test01, res
    integer :: i, j, k
    integer :: nx, ny, nz, t
    double precision :: lx, ly, lz
    
    test01 = .TRUE.
    do i = 7,7
       do j = 7,7
          do k = 7,7

             nx = 2**i
             ny = 2**j
             nz = 2**k
             lx = 1.0d0
             ly = 1.0d0
             lz = 1.0d0
             t = 1

             ! Actually test is calling here
             if ((nx > 0) .and. (ny > 0) .and. (nz > 0) .and. (t > 0) .and. &
                  & (lx > EPS) .and. (ly > EPS) .and. (lz > EPS)) then
                res = runtest(nx, ny, nz, lx, ly, lz, t)
             else
                res = .FALSE.
             endif

             if (res .eqv. .TRUE.) then 
                print '(A,"Test (",I4,I4,I4,") passed!")', STR_OK, nx, ny, nz
                test01 = test01 .and. .TRUE.
             else
                print '(A,"Test (",I4,I4,I4,") failed!")', STR_FAIL, nx, ny, nz
                test01 = .FALSE.
             endif
          end do
       end do
    end do

  end function test01

  function runtest(nx, ny, nz, lx, ly, lz, t)
    implicit none

    logical :: runtest
    integer, intent(in) :: nx, ny, nz, t
    double precision, intent(in) :: lx, ly, lz

    double precision, pointer :: u(:,:,:), rho(:,:,:), u0(:,:,:)
    double precision :: hx, hy, hz
    integer :: total_size, err_code
    double precision :: coef1, coef2, coef3
    integer :: i, j, k
    double precision :: errval = 0.0d0

#ifdef CPUTIMER
    real :: start, finish
#endif

#ifdef CPUTIMER
    call cpu_time(start)
#endif
    
    print *, "========================================"
    print *, "Parameters:"
    print '(" Area size:            L = ",f7.4,f7.4,f7.4)',lx,ly,lz
    print '(" Number of points:     N = ",i5,i5,i5)',nx,ny,nz
    print *, "========================================"


    hx = lx/nx; hy = ly/ny; hz = lz/nz
    total_size = nx*ny*nz

    !======================= Memory allocation ==================================
    allocate (u(0:nx,0:ny,0:nz))
    allocate (u0(0:nx,0:ny,0:nz))
    allocate (rho(0:nx,0:ny,0:nz))
    !============================================================================

    !======================= Initialization =====================================
    u = 0.0
    coef1 = 1.0
    coef2 = 1.0
    coef3 = 1.0

    do k = 0, nz
       do j = 0, ny
          do i = 0, nx
             u0(i,j,k) = dsin(coef1*pi*i/nx) * &
                  & dsin(coef2*pi*j/ny) * dsin(coef3*pi*k/nz)
             rho(i,j,k) = u0(i,j,k)*( &
                  & ( coef1*coef1 + coef2*coef2 + coef3*coef3 )*pi*pi )
          end do
       end do
    end do
    !============================================================================

    !========================= Solve problem ====================================
    err_code = dpois0cufft3d_init (nx, ny, nz, lx, ly, lz)

    do i=1,t
#ifdef TALKATIVE
       print '(" ==== Iteration #",I0.5," ====")',i
#endif
       err_code = dpois0cufft3d (u(0:nx-1,0:ny-1,0:nz-1), &
            & rho(0:nx-1,0:ny-1,0:nz-1), nx, ny, nz, lx, ly, lz)
#ifdef TALKATIVE
       print '(" ========== Done ==========")'
#endif
    end do
    
    err_code = dpois0cufft3d_fin ()
    !============================================================================

    !============================= Output =======================================
#ifndef NOOUTPUT
    if (err_code == 0) then
       open(1,file="out/u.bin",form="unformatted")
       open(2,file="out/u0.bin",form="unformatted")
       open(3,file="out/rho.bin",form="unformatted")

       i = nx/2;
       j = ny/2;
       do k = 0, nz
          write(1) real(k*hx), real(u(i,j,k))
          write(2) real(k*hx), real(u0(i,j,k))
          write(3) real(k*hx), real(rho(i,j,k))
       end do
    end if
#endif
    !============================================================================

    !==================== Comparising with exact solution =======================
    if (err_code == 0) then
       errval = maxval(abs(u - u0))
#ifdef TALKATIVE
       print *,"[ERRVAL] Max error: ",errval
       print *,"[ERRVAL] Max error location: ",maxloc(abs(u - u0))
#endif
    end if
    !============================================================================

    !==================== Memory deallocation ===================================
    deallocate (u)
    deallocate (u0)
    deallocate (rho)
    !============================================================================

#ifdef CPUTIMER
    call cpu_time(finish) 
    print '(" [CPUTIMER] Test total CPU+GPU time = ",f6.3," seconds.")',finish-start
#endif

    !==================== Test result ===========================================
    if ((err_code == 0) .and. (errval < EPS)) then
       runtest = .TRUE.
    else
       runtest = .FALSE.
    end if
    !============================================================================

  end function runtest

end program
