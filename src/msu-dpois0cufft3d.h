/**
 * \file msu-dpois0cufft3d.h
 * \brief Headers for Poisson equation solver. 
 * \note C++ header files.
 * \author Nikolai Nikishin <truecoldfault@gmail.com>
 * \copyright Department of the Automation for Scientific Research CMC MSU.
 * \date 2013
 * \warning CUFFT does not normalize the results!
 * (IFFT(FFT(A))) = len(A)*A. Use msu_cufft_normalize() function.
 * \warning CUDA 2.x or higher is requried for double precision calculations.
 **/

/************************** Main interface *************************************/
extern "C" int 
MSU_dpois0cufft3d(double *u, const double *rho, 
		  const int nx, const int ny, const int nz, 
		  const double lx, const double ly, const double lz);

extern "C" int 
MSU_dpois0cufft3d_init(const int nx, const int ny, const int nz, 
		       const double lx, const double ly, const double lz);
extern "C" int 
MSU_dpois0cufft3d_fin(void);
/*******************************************************************************/

/*************************** Device code ***************************************/
extern "C" __global__ void 
MSU_dpois0cufft3d_eigenmul(cufftDoubleComplex *rho, cufftDoubleReal *lambdabuf,
			   const int nx, const int ny, const int nz);

extern "C" __global__ void 
MSU_dpois0cufft3d_eigencalc(cufftDoubleReal *lambdabuf, 
			     const int nx, const int ny, const int nz,
			     const double hx, const double hy, const double hz,
			     const int total_size);

extern "C" __global__ void 
MSU_dpois0cufft3d_expand(cufftDoubleComplex *outdata, cufftDoubleReal *indata,
			 const int nx, const int ny, const int nz);

extern "C" __global__ void 
MSU_dpois0cufft3d_restrict(cufftDoubleReal *outdata, cufftDoubleComplex *indata,
			 const int nx, const int ny, const int nz);
/*******************************************************************************/

/************ CUPRINTF kernel printing  macro **********************************/
/**
 * \brief Let you printf() within CUDA kernels.
 * \note Use like printf(), but you should pass some value! 
 **/
#define CUPRINTF(fmt, ...) printf("[%d, %d]:\t" fmt, \
          blockIdx.y*gridDim.x+blockIdx.x,\
          threadIdx.z*blockDim.x*blockDim.y+threadIdx.y*blockDim.x+threadIdx.x,\
				  __VA_ARGS__)
/*******************************************************************************/

/************** Error handling macros ******************************************/
/**
 * \brief Error string returning function for CUFFT error handling macros.
 * \param[in] error cufftResult code.
 * \result Returns error string for error codes.
 **/
 static const char *cufftGetErrorString(cufftResult error)
{
     switch (error)
     {
     case CUFFT_SUCCESS:
	  return "CUFFT_SUCCESS";

     case CUFFT_INVALID_PLAN:
	  return "CUFFT_INVALID_PLAN";

     case CUFFT_ALLOC_FAILED:
	  return "CUFFT_ALLOC_FAILED";

     case CUFFT_INVALID_TYPE:
	  return "CUFFT_INVALID_TYPE";

     case CUFFT_INVALID_VALUE:
	  return "CUFFT_INVALID_VALUE";

     case CUFFT_INTERNAL_ERROR:
	  return "CUFFT_INTERNAL_ERROR";

     case CUFFT_EXEC_FAILED:
	  return "CUFFT_EXEC_FAILED";

     case CUFFT_SETUP_FAILED:
	  return "CUFFT_SETUP_FAILED";

     case CUFFT_INVALID_SIZE:
	  return "CUFFT_INVALID_SIZE";

     case CUFFT_UNALIGNED_DATA:
	  return "CUFFT_UNALIGNED_DATA";
     }

     return "<unknown>";
}

/**
 * \brief Error handling macros for CUDA Runtime calls.
 * \result Calls function and checks return code. Aborts if call failed.
 **/
#define cudaSafeCall(err) { __cudaSafeCall(err, __FILE__, __LINE__); }
inline void __cudaSafeCall(cudaError_t code, const char *file, 
			   const int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr, "CUDA error in file <%s>, line %i. Code: %s\n", file, 
	      line, cudaGetErrorString(code));
      if (abort) exit(code);
   }
}

/**
 * \brief Error handling macros for CUDA CUFFT calls.
 * \result Calls function and checks return code. Aborts if call failed.
 **/
#define cufftSafeCall(err) { __cufftSafeCall(err, __FILE__, __LINE__); }
inline void __cufftSafeCall(cufftResult code, const char *file, 
			    const int line, bool abort=true)
{
     if (code != CUFFT_SUCCESS) 
     {
	  fprintf(stderr, "CUFFT error in file <%s>, line %i. Code: %s\n", file, 
		  line, cufftGetErrorString(code));
	  if (abort) exit(code);
   }
}
/*******************************************************************************/
