/**
 * \file msu-dpois0cufft3d.cu
 * \brief Solver of Poisson equation with Dirichlet's boundary conditions in 
 * 3d-space using CUFFT library.
 * \note CUDA C version.
 * \author Nikolai Nikishin, <truecoldfault@gmail.com>
 * \copyright Department of the Automation for Scientific Research CMC MSU
 * \date 2013
 * \warning CUFFT does not normalize the results!
 * (IFFT(FFT(A))) = len(A)*A. Use msu_cufft_normalize() function.
 * \warning CUDA 2.x or higher is requried for double precision calculations.
 * \todo Separate CUFFT calculations in its own stream.
 **/

/// Number of streams.
#define STREAMSNUM 2

#include <cufft.h>
#include <math_constants.h>
#include "msu-dpois0cufft3d.h"

/************************** Global variables ***********************************/
dim3 threadsPerBlock, blocksPerGrid;
cufftHandle plan;
cudaStream_t stream[STREAMSNUM];
cufftDoubleReal *databuf,*lambdabuf;
cufftDoubleComplex *fftbuf;

#ifdef GPUTIMER
cudaEvent_t start,stop;
double total_time = 0.0;
float time_cur;
#endif
#ifdef GPUTIMER_DETAILED
float time_old;
#endif
/*******************************************************************************/

/**
 * \brief Poisson equation solver. Initializing function.
 * \description Setting CUDA grid and block sizes properly. Creating CUDA 
 * streams. Allocating memory. Calculating eigenvalues. Creating CUFFT plan.
 * \param[in] nx Size of source array in x dimesion.
 * \param[in] ny Size of source array in y dimesion.
 * \param[in] nz Size of source array in z dimesion.
 * \param[in] lx Size of area in x dimesion.
 * \param[in] ly Size of area in y dimesion.
 * \param[in] lz Size of area in z dimesion.
 * \retval 0 Successful.
 * \retval -1 Execution failed. 
 **/
extern "C" int MSU_dpois0cufft3d_init(const int nx, const int ny, const int nz, 
			      const double lx, const double ly, const double lz)
{
    const int total_size = nx*ny*nz;
    const double hx = lx / (double) nx;
    const double hy = ly / (double) ny;
    const double hz = lz / (double) nz;
    
    int deviceCount = 0, deviceNum, isDeviceChosen = 0;
    cudaDeviceProp deviceProp;
    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

    /************************* Choosing device  *******************************/
    if (deviceCount <= 0)
    {
#ifdef TALKATIVE
        printf(" [ERROR] There are no available device(s) that support CUDA\n");
#endif
	return EXIT_FAILURE;
    }
    for (deviceNum=0; deviceNum < deviceCount && isDeviceChosen==0; ++deviceNum)
    {
        cudaSetDevice(deviceNum);
        cudaGetDeviceProperties(&deviceProp, deviceNum);

	if (deviceProp.major >= 2)
	{
#ifdef TALKATIVE
	    printf(" [INFO] Using device # %d\n",deviceNum);
#endif
	    isDeviceChosen = 1;			       
	}
	else
	{
#ifdef TALKATIVE
	    printf(" [INFO] Device #%d doesn't have Compute Capability 2.0+\n",
		   deviceNum);
#endif
	}
    }
    if (isDeviceChosen == 0)
    {
#ifdef TALKATIVE
        printf(" [ERROR] There are no available device(s) that support " \
	    "Compute Capability 2.0+\n");
#endif
	return EXIT_FAILURE;
    }
    /***************************************************************************/
#ifdef TALKATIVE
    printf(" [RUNNING] Solver initialization... \n");
#endif

#ifdef TALKATIVE
    printf(" [INFO] Problem sizes: %d %d %d \n",nx,ny,nz);
#endif

    threadsPerBlock.x = min(nx, deviceProp.maxThreadsPerBlock);
    threadsPerBlock.y = min(ny, (deviceProp.maxThreadsPerBlock - 1)/threadsPerBlock.x + 1);
    threadsPerBlock.z = min(nz, (deviceProp.maxThreadsPerBlock - 1)/ 
			    (threadsPerBlock.x*threadsPerBlock.y) + 1);
    
    blocksPerGrid.x = (nx - 1)/threadsPerBlock.x + 1;
    blocksPerGrid.y = (ny - 1)/threadsPerBlock.y + 1;
    blocksPerGrid.z = (nz - 1)/threadsPerBlock.z + 1;

#ifdef TALKATIVE
    printf(" [INFO] CUDA. maxThreadsPerBlock: %d \n", deviceProp.maxThreadsPerBlock);
    printf(" [INFO] CUDA. threadsPerBlock: %d %d %d \n",
	threadsPerBlock.x, threadsPerBlock.y, threadsPerBlock.z);
    printf(" [INFO] CUDA. blocksPerGrid: %d %d %d \n",
	blocksPerGrid.x, blocksPerGrid.y, blocksPerGrid.z);
#endif

    for (int i = 0; i < 2; ++i)
	cudaStreamCreate(&stream[i]);

    /************************* Memory allocation *******************************/
    cudaSafeCall( cudaMalloc((void**)&lambdabuf, 
			     sizeof(cufftDoubleReal)*total_size) );
    cudaSafeCall( cudaMalloc((void**)&databuf, 
			     sizeof(cufftDoubleReal)*total_size) );
    cudaSafeCall( cudaMalloc((void**)&fftbuf, 
			     sizeof(cufftDoubleComplex)*(total_size<<3)) );
    /***************************************************************************/

    /******************** Eigenvalues calculate kernel *************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUDA kernel. Eigenvalues calculation... \n");
#endif
    MSU_dpois0cufft3d_eigencalc<<<blocksPerGrid,threadsPerBlock>>>(lambdabuf, 
								    nx, ny, nz, 
								    hx, hy, hz, 
								    total_size);
    cudaSafeCall( cudaPeekAtLastError() );
    /***************************************************************************/

    cufftSafeCall( cufftPlan3d(&plan, nx<<1, ny<<1, nz<<1, CUFFT_Z2Z) );
    cufftSafeCall( cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE));

#ifdef GPUTIMER
    cudaSafeCall( cudaEventCreate(&start) );
    cudaSafeCall( cudaEventCreate(&stop) );

    printf(" [GPUTIMER] Grid size: (%d, %d, %d)\n",nx,ny,nz);
#endif
#ifdef TALKATIVE
    printf(" [RUNNING] Solver initialization... done. \n");
#endif
    return EXIT_SUCCESS;
}

/**
 * \brief Poisson equation solver. Finalizing function.
 * \description CUDA streams destruction. Memory deallocation.
 * \result Program prepared for shutting down correctly.
 * \retval 0 Successful.
 * \retval -1 Execution failed. 
 **/
extern "C" int MSU_dpois0cufft3d_fin(void) 
{
#ifdef TALKATIVE
    printf(" [RUNNING] Solver finalization... \n");
#endif    
#ifdef GPUTIMER
    cudaSafeCall( cudaEventSynchronize(stop) );
#endif

    for (int i = 0; i < 2; ++i)
	cudaStreamDestroy(stream[i]);
    /************************* Memory deallocation *****************************/
    cudaSafeCall( cudaFree(databuf) );
    cudaSafeCall( cudaFree(fftbuf) ); 
    cudaSafeCall( cudaFree(lambdabuf) ); 
    cufftSafeCall( cufftDestroy(plan) );
    /***************************************************************************/

#ifdef GPUTIMER
    cudaSafeCall( cudaEventDestroy(start) );
    cudaSafeCall( cudaEventDestroy(stop) );
    printf(" [GPUTIMER] Total CUDA C time: %f\n",total_time);
#endif
#ifdef TALKATIVE
    printf(" [RUNNING] Solver finalization... done. \n");
#endif    

    return EXIT_SUCCESS;
}

/**
 * \brief  Poisson equation solver. General function.
 * \param[out] u Array for solution. Column-majored.
 * \param[in] rho Right part array. Column-majored.
 * \param[in] nx Size of arrays in x dimesion.
 * \param[in] ny Size of arrays in y dimesion.
 * \param[in] nz Size of arrays in z dimesion.
 * \param[in] lx Size of area in x dimesion.
 * \param[in] ly Size of area in y dimesion.
 * \param[in] lz Size of area in z dimesion.
 * \result Solution is placed in solution array.
 * \retval 0 Successful.
 * \retval -1 Execution failed. 
 * \warning CUFFT does not normalize the results! (IFFT(FFT(A))) = len(A)*A.
 **/
extern "C" int MSU_dpois0cufft3d(double *u, const double *rho,
				 const int nx, const int ny, const int nz, 
			       const double lx, const double ly, const double lz)
{
    const int total_size = nx*ny*nz;

#ifdef TALKATIVE
    printf(" [RUNNING] Poisson solver with cufft... \n");
#endif
#ifdef GPUTIMER
    cudaSafeCall( cudaEventRecord(start, 0) );
#endif
#ifdef GPUTIMER_DETAILED
    time_old = 0.0;
#endif

    /******************** Copying data to device *******************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUDA. Copying data to device... \n");
#endif
    cudaSafeCall( cudaMemcpyAsync(databuf, rho, 
				  sizeof(cufftDoubleReal)*total_size, 
				  cudaMemcpyHostToDevice, stream[0]) );

    cudaSafeCall( cudaMemsetAsync(fftbuf, 0, 
				  sizeof(cufftDoubleComplex)*(total_size<<3),
				  stream[1]) );
    /***************************************************************************/

    /******************** Odd data extention ***********************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUDA kernel. Expanding data... \n");
#endif
    MSU_dpois0cufft3d_expand<<<blocksPerGrid, threadsPerBlock>>>(fftbuf, databuf, 
								nx, ny, nz);
    cudaSafeCall( cudaPeekAtLastError() );
    /***************************************************************************/

#ifdef GPUTIMER_DETAILED
    cudaSafeCall( cudaEventRecord(stop, 0) );
    cudaSafeCall( cudaEventSynchronize(stop) );
    cudaSafeCall( cudaEventElapsedTime(&time_cur, start, stop) );
    printf(" [GPUTIMER_DETAILED] Expand kernel Time: %f\n",time_cur-time_old);
    time_old = time_cur;
#endif

    /********************** Forward Fourier Transform **************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUFFT. Forward transmorm... \n");
#endif

    cufftSafeCall( cufftExecZ2Z(plan, fftbuf, fftbuf, CUFFT_FORWARD) );
    /***************************************************************************/

#ifdef GPUTIMER_DETAILED
    cudaSafeCall( cudaEventRecord(stop, 0) );
    cudaSafeCall( cudaEventSynchronize(stop) );
    cudaSafeCall( cudaEventElapsedTime(&time_cur, start, stop) );
    printf(" [GPUTIMER_DETAILED] Forward FFT Time: %f\n",time_cur-time_old);
    time_old = time_cur;
#endif

    /*************************** Solver ****************************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUDA kernel. Divide by eigenvalues... \n");
#endif

    MSU_dpois0cufft3d_eigenmul<<<blocksPerGrid, threadsPerBlock>>>(
	fftbuf, lambdabuf, nx, ny, nz);
    cudaSafeCall( cudaPeekAtLastError() );
    /***************************************************************************/

#ifdef GPUTIMER_DETAILED
    cudaSafeCall( cudaEventRecord(stop, 0) );
    cudaSafeCall( cudaEventSynchronize(stop) );
    cudaSafeCall( cudaEventElapsedTime(&time_cur, start, stop) );
    printf(" [GPUTIMER_DETAILED] Solver Time: %f\n",time_cur-time_old);
    time_old = time_cur;
#endif

    /********************** Inverse transform **********************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUFFT. Inverse transform... \n");
#endif
    cufftSafeCall( cufftExecZ2Z(plan, fftbuf, fftbuf, CUFFT_INVERSE) );
    /***************************************************************************/

#ifdef GPUTIMER_DETAILED
    cudaSafeCall( cudaEventRecord(stop, 0) );
    cudaSafeCall( cudaEventSynchronize(stop) );
    cudaSafeCall( cudaEventElapsedTime(&time_cur, start, stop) );
    printf(" [GPUTIMER_DETAILED] Inverse FFT Time: %f\n",time_cur-time_old);
    time_old = time_cur;
#endif

    /********************** Data restriction ***********************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUDA kernel. Reduce data... \n");
#endif

    MSU_dpois0cufft3d_restrict<<<blocksPerGrid, threadsPerBlock>>>(databuf, fftbuf, 
								nx, ny, nz);
    cudaSafeCall( cudaPeekAtLastError() );
    /***************************************************************************/

#ifdef GPUTIMER_DETAILED
    cudaSafeCall( cudaEventRecord(stop, 0) );
    cudaSafeCall( cudaEventSynchronize(stop) );
    cudaSafeCall( cudaEventElapsedTime(&time_cur, start, stop) );
    printf(" [GPUTIMER_DETAILED] Reduce Time: %f\n",time_cur-time_old);
    time_old = time_cur;
#endif

    /********************** Copying data to host *******************************/
#ifdef TALKATIVE
    printf(" [RUNNING] CUDA. Copying data to host... \n");
#endif
    cudaSafeCall( cudaMemcpy(u, databuf, sizeof(cufftDoubleReal)*total_size, 
			     cudaMemcpyDeviceToHost) );
    /***************************************************************************/

#ifdef TALKATIVE
    printf(" [RUNNING] Poisson solver with cufft... Done! \n");
#endif
#ifdef GPUTIMER
    cudaSafeCall( cudaEventRecord(stop, 0) );
    cudaSafeCall( cudaEventSynchronize(stop) );
    cudaSafeCall( cudaEventElapsedTime(&time_cur, start, stop) );
    total_time += time_cur;
#endif
#ifdef GPUTIMER_DETAILED
    printf(" [GPUTIMER_DETAILED] CUDA C iteration time: %f\n",time_cur);
#endif

    return EXIT_SUCCESS;  
}

/*******************************************************************************/
/***************************** Device code *************************************/
/*******************************************************************************/

/**
 * \brief CUDA kernel. Solve poisson equation in frequency space.
 * \decription Multiple Fourier coefficients of right part on corresponding 
 * coefficients before inverse FFT.
 * \param[out] rho Extended array with Fourier coefficients of right part. 
 * Row-major order.
 * \param[in] lambdabuf Coefficients calculated by MSU_dpois0cufft3d_eigencalc
 * CUDA kernel. Column-major order.
 * \param[in] nx Size of arrays in x dimesion.
 * \param[in] ny Size of arrays in y dimesion.
 * \param[in] nz Size of arrays in z dimesion.
 * \result New coefficients placed in right part array.
 **/
__global__ void MSU_dpois0cufft3d_eigenmul(cufftDoubleComplex *rho, 
					   cufftDoubleReal *lambdabuf,
					   const int nx, 
					   const int ny,
					   const int nz) 
{
    const int i1 = threadIdx.x + blockDim.x * blockIdx.x;
    const int j1 = threadIdx.y + blockDim.y * blockIdx.y;
    const int k1 = threadIdx.z + blockDim.z * blockIdx.z;

    const int nx2 = nx<<1;
    const int ny2 = ny<<1;
    const int nz2 = nz<<1;
    const int i2 = nx2 - i1;
    const int j2 = ny2 - j1;
    const int k2 = nz2 - k1;

    double oneolambda;

    if( (i1 > 0 && j1 > 0  && k1 > 0) && (i1 < nx && j1 < ny  && k1 < nz) )
    {
	oneolambda = lambdabuf[i1 + nx*(j1 + ny*k1)]; 

//	rho[k1 + nz2*(j1 + ny2*i1)].x *= oneolambda;
	rho[k1 + nz2*(j1 + ny2*i1)].y *= oneolambda;

//	rho[k2 + nz2*(j1 + ny2*i1)].x *= oneolambda;
	rho[k2 + nz2*(j1 + ny2*i1)].y *= oneolambda;

//	rho[k1 + nz2*(j2 + ny2*i1)].x *= oneolambda;
	rho[k1 + nz2*(j2 + ny2*i1)].y *= oneolambda;

//	rho[k1 + nz2*(j1 + ny2*i2)].x *= oneolambda;
	rho[k1 + nz2*(j1 + ny2*i2)].y *= oneolambda;

//	rho[k2 + nz2*(j2 + ny2*i1)].x *= oneolambda;
	rho[k2 + nz2*(j2 + ny2*i1)].y *= oneolambda;

//	rho[k2 + nz2*(j1 + ny2*i2)].x *= oneolambda;
	rho[k2 + nz2*(j1 + ny2*i2)].y *= oneolambda;

//	rho[k1 + nz2*(j2 + ny2*i2)].x *= oneolambda;
	rho[k1 + nz2*(j2 + ny2*i2)].y *= oneolambda;

//	rho[k2 + nz2*(j2 + ny2*i2)].x *= oneolambda;
	rho[k2 + nz2*(j2 + ny2*i2)].y *= oneolambda;
    }
    return;
}

/**
 * \brief CUDA kernel. Odd extension of data for CUFFT.
 * \param[out] extended Array for extended data storage. Should have doubled size 
 * of source on each dimension. Row-major order.
 * \param[in] source Array with data that should be extended. Column-major order.
 * \param[in] nx Size of source array in x dimesion.
 * \param[in] ny Size of source array in y dimesion.
 * \param[in] nz Size of source array in z dimesion.
 * \result Extended data is placed in proper array.
 **/
__global__ void MSU_dpois0cufft3d_expand( cufftDoubleComplex *extended, 
					 cufftDoubleReal *source, 
					 const int nx, 
					 const int ny,
					 const int nz) 
{
    const int i1 = threadIdx.x + blockDim.x * blockIdx.x;
    const int j1 = threadIdx.y + blockDim.y * blockIdx.y;
    const int k1 = threadIdx.z + blockDim.z * blockIdx.z;

    const int nx2 = nx<<1;
    const int ny2 = ny<<1;
    const int nz2 = nz<<1;

    const int i2 = nx2 - i1;
    const int j2 = ny2 - j1;
    const int k2 = nz2 - k1;

    if( (i1 > 0 && j1 > 0  && k1 > 0) && (i1 < nx && j1 < ny  && k1 < nz) )
    {
	const double elem = source[i1 + nx*(j1 + ny*k1)];

	extended[k1 + nz2*(j1 + ny2*i1)].x = +elem;
	extended[k2 + nz2*(j1 + ny2*i1)].x = -elem;
	extended[k1 + nz2*(j2 + ny2*i1)].x = -elem;
	extended[k1 + nz2*(j1 + ny2*i2)].x = -elem;
	extended[k2 + nz2*(j2 + ny2*i1)].x = +elem;
	extended[k2 + nz2*(j1 + ny2*i2)].x = +elem;
	extended[k1 + nz2*(j2 + ny2*i2)].x = +elem;
	extended[k2 + nz2*(j2 + ny2*i2)].x = -elem;
    }

    return;
}

/**
 * \brief CUDA kernel. Copying proper part of data from extended array.
 * \param[out] restricted Array for restriction data. Column-major order.
 * \param[in] extended Source array with extended data. Should have doubled size 
 * of restriction array on each dimension. Row-major order.
 * \param[in] nx Size of restriction array in x dimesion.
 * \param[in] ny Size of restriction array in y dimesion.
 * \param[in] nz Size of restriction array in z dimesion.
 * \result Proper part of data in output array.
 **/
__global__ void MSU_dpois0cufft3d_restrict( cufftDoubleReal *restricted, 
					 cufftDoubleComplex *extended, 
					 const int nx, 
					 const int ny,
					 const int nz) 
{
    const int i = threadIdx.x + blockDim.x * blockIdx.x;
    const int j = threadIdx.y + blockDim.y * blockIdx.y;
    const int k = threadIdx.z + blockDim.z * blockIdx.z;

//    const int nx2 = nx<<1;
    const int ny2 = ny<<1;
    const int nz2 = nz<<1;

    if( (i > 0 && j > 0  && k > 0) && (i < nx && j < ny  && k < nz) )
	restricted[i + nx*(j + ny*k)] = extended[k + nz2*(j + ny2*i)].x;
    return;
}

/**
 * \brief CUDA kernel. Calculating coefficients for solver of Poisson equation
 * with Dirichlet's boundary conditions.
 * \description Calculate eigenvalues of Laplace operator. Divide one by these 
 * eigenvalues multiplied on total array size for normalization.
 * \param[out] lambdabuf Array for coefficients. Column-major order.
 * \param[in] nx Size of array in x dimesion.
 * \param[in] ny Size of array in y dimesion.
 * \param[in] nz Size of array in z dimesion.
 * \param[in] hx Size of step in x diretion.
 * \param[in] hy Size of step in x diretion.
 * \param[in] hz Size of step in x diretion.
 * \param[in] total_size Total number of elements.
 * \result Proper coefficients are placed in output array.
 **/
__global__ void MSU_dpois0cufft3d_eigencalc( cufftDoubleReal *lambdabuf,
					      const int nx, 
					      const int ny,
					      const int nz,
					      const double hx, 
					      const double hy, 
					      const double hz,
					      const int total_size) 
{
    const int i = threadIdx.x + blockDim.x * blockIdx.x;
    const int j = threadIdx.y + blockDim.y * blockIdx.y;
    const int k = threadIdx.z + blockDim.z * blockIdx.z;

    double sin1, sin2, sin3;
    if( (i > 0 && j > 0 && k > 0) && (i < nx && j < ny && k < nz) ) 
    {
    	/***********************************************************************
	*    lambda = [2 sin (k pi / 2N ) / h ]^2
	*    oneolambda = 1/lambda = [h / 2 sin (k pi / 2 N ) ]^2
	*    cuda constant: CUDART_PIO2 = pi/2
	************************************************************************/
	sin1 = sin( CUDART_PIO2*i / nx ) /  hx;
	sin2 = sin( CUDART_PIO2*j / ny ) /  hy;
	sin3 = sin( CUDART_PIO2*k / nz ) /  hz;
	/* Magic number below is 1/32 */
	lambdabuf[i + nx*(j + ny*k)] = 0.03125 / ( 
	    total_size*(sin1*sin1 + sin2*sin2 + sin3*sin3) );
    }
    return;
}

/*******************************************************************************/
/************************* End of device code **********************************/
/*******************************************************************************/
