!================================================================================
!> @file msu-dpois0cufft3d.f90
!! @brief Fortran90 interface module for solver of Poisson equation with 
!! Dirichlet's boundary conditions in 3d-space using CUFFT library.
!! @author Nikolai Nikishin, <truecoldfault@gmail.com>
!! @copyright Department of the Automation for Scientific Research CMC MSU
!! @date 2013
!! @warning Compiler should support iso_c_binding module (Fortran 2003).
!! Gfortran 4.7.2 fits.
!================================================================================

module msu_dpois0cufft3d_mod
  interface
!================================================================================
!> @brief Poisson equation solver. General function.
!! @note This is Fortran interface for CUDA C implementation.
!! @param[out] u Array for solution.
!! @param[in] rho Right part array.
!! @param[in] nx Size of arrays in x dimesion.
!! @param[in] ny Size of arrays in y dimesion.
!! @param[in] nz Size of arrays in z dimesion.
!! @param[in] lx Size of area in x dimesion.
!! @param[in] ly Size of area in y dimesion.
!! @param[in] lz Size of area in z dimesion.
!! @result Solution is placed in solution array.
!! @retval 0 Successful.
!! @retval -1 Execution failed. 
!! @warning CUFFT does not normalize the results! (IFFT(FFT(A))) = len(A)*A.
!================================================================================
     integer (C_INT) function dpois0cufft3d (u, rho, nx, ny, nz, &
          & lx, ly, lz) bind(C,name="MSU_dpois0cufft3d")
       use iso_c_binding
       implicit none 
       real (C_DOUBLE) :: u(*)
       real (C_DOUBLE) :: rho(*)
       integer (C_INT), value :: nx
       integer (C_INT), value :: ny
       integer (C_INT), value :: nz
       real (C_DOUBLE), value :: lx
       real (C_DOUBLE), value :: ly
       real (C_DOUBLE), value :: lz
     end function dpois0cufft3d

!================================================================================
!> @brief Poisson equation solver. Initializing function.
!! @description Setting CUDA grid and block sizes properly. Creating CUDA 
!! streams. Allocating memory. Calculating eigenvalues. Creating CUFFT plan.
!! @note This is Fortran interface for CUDA C implementation.
!! @param[in] nx Size of source array in x dimesion.
!! @param[in] ny Size of source array in y dimesion.
!! @param[in] nz Size of source array in z dimesion.
!! @param[in] lx Size of area in x dimesion.
!! @param[in] ly Size of area in y dimesion.
!! @param[in] lz Size of area in z dimesion.
!! @retval 0 Successful.
!! @retval -1 Execution failed. 
!================================================================================
     integer (C_INT) function dpois0cufft3d_init (nx, ny, nz, &
          & lx, ly, lz) bind(C,name="MSU_dpois0cufft3d_init")
       use iso_c_binding
       implicit none 
       integer (C_INT), value :: nx
       integer (C_INT), value :: ny
       integer (C_INT), value :: nz
       real (C_DOUBLE), value :: lx
       real (C_DOUBLE), value :: ly
       real (C_DOUBLE), value :: lz
     end function dpois0cufft3d_init

!================================================================================
!> @brief Poisson equation solver. Finalizing function.
!! @description CUDA streams destruction. Memory deallocation.
!! @note This is Fortran interface for CUDA C implementation.
!! @result Program prepared for shutting down correctly.
!! @retval 0 Successful.
!! @retval -1 Execution failed. 
!================================================================================
     integer (C_INT) function dpois0cufft3d_fin () &
          & bind(C,name="MSU_dpois0cufft3d_fin")
       use iso_c_binding
       implicit none 
     end function dpois0cufft3d_fin
  end interface
end module msu_dpois0cufft3d_mod
